const avengersNames = [
  'Thor',
  'Cap',
  'Tony Stark',
  'Black Panther',
  'Black Widow',
  'Hulk',
  'Spider-Man',
]
let randomName = avengersNames[Math.floor(Math.random() * avengersNames.length)]

const main = async () => {
  /* Event handlers */

  // When a stream is added to the conference
  VoxeetSDK.conference.on('streamAdded', (participant, stream) => {
    if (stream.type === 'ScreenShare') {
      return addScreenShareNode(stream)
    }

    if (stream.getVideoTracks().length) {
      // Only add the video node if there is a video track
      addVideoNode(participant, stream)
    }

    addParticipantNode(participant)
  })

  VoxeetSDK.notification.on('invitation', (e) => {
    console.log(e)
  })

  // When a message is added to the conference
  VoxeetSDK.command.on('received', (participant, message) => {
    addChatMessageNode(participant, message)
  })

  // When a stream is updated
  VoxeetSDK.conference.on('streamUpdated', (participant, stream) => {
    if (stream.type === 'ScreenShare') return

    if (stream.getVideoTracks().length) {
      // Only add the video node if there is a video track
      addVideoNode(participant, stream)
    } else {
      removeVideoNode(participant)
    }
  })

  // When a stream is removed from the conference
  VoxeetSDK.conference.on('streamRemoved', (participant, stream) => {
    if (stream.type === 'ScreenShare') {
      return removeScreenShareNode()
    }

    removeVideoNode(participant)
    removeParticipantNode(participant)
  })

  // When permissions are updated
  VoxeetSDK.conference.on('permissionsUpdated', (data) => {
    console.log('PERMISSIONS UPDATED EVENT')
  })

  // For local use credentials

  // No-security-default
  let APP_ID = 'gGzW67pd7xpNo6iIuAnHHQ=='
  let SECRET_ID = 'FtXhRGoKop1dqsJL01RfPNdK4aoXQGs89hhiiAsvWlI='

  // Security
  // let APP_ID = 'Cy-kNqIn6r5P9hrEP8oVrg=='
  // let SECRET_ID = 't1Y0wYqXssp_m9PgU88_pAvUEFbwT6fDtoyxmJUn5fo='

  for (const [key, value] of new URLSearchParams(window.location.search).entries()) {
    if (key === 'APP_ID') {
      APP_ID = value
    }
    if (key === 'SECRET_ID') {
      SECRET_ID = value
    }
  }

  try {
    // Initialize the Voxeet SDK
    // WARNING: It is best practice to use the VoxeetSDK.initializeToken function to initialize the SDK.
    // Please read the documentation at:
    // https://docs.dolby.io/interactivity/docs/initializing
    alert(`initializing with credentials: \nAPP_ID: ${APP_ID}\nSECRET_ID: ${SECRET_ID}`)
    VoxeetSDK.initialize(APP_ID, SECRET_ID)

    // Open a session for the user
    await VoxeetSDK.session.open({ name: randomName, externalId: 'externalId-123' })

    // Initialize the UI
    initUI()
  } catch (e) {
    alert('Something went wrong : ' + e)
  }
}

main()
