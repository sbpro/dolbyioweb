const initUI = () => {
  const nameMessage = document.getElementById('name-message')
  const joinButton = document.getElementById('join-btn')
  const conferenceAliasInput = document.getElementById('alias-input')
  const messageInput = document.getElementById('message-input')
  const sendMessageBtn = document.getElementById('send-msg-btn')
  const leaveButton = document.getElementById('leave-btn')
  const inviteInput = document.getElementById('invite-input')
  const inviteButton = document.getElementById('invite-btn')
  const lblDolbyVoice = document.getElementById('label-dolby-voice')
  const startVideoBtn = document.getElementById('start-video-btn')
  const stopVideoBtn = document.getElementById('stop-video-btn')
  const startAudioBtn = document.getElementById('start-audio-btn')
  const stopAudioBtn = document.getElementById('stop-audio-btn')
  const startScreenShareBtn = document.getElementById('start-screenshare-btn')
  const stopScreenShareBtn = document.getElementById('stop-screenshare-btn')
  const startRecordingBtn = document.getElementById('start-recording-btn')
  const stopRecordingBtn = document.getElementById('stop-recording-btn')
  const permissionInviteBtn = document.getElementById('INVITE')
  const permissionKickBtn = document.getElementById('KICK')
  const permissionUpdatePermissionBtn = document.getElementById('UPDATE_PERMISSIONS')
  const permissionJoinBtn = document.getElementById('JOIN')
  const permissionSendAudioBtn = document.getElementById('SEND_AUDIO')
  const permissionSendVideoBtn = document.getElementById('SEND_VIDEO')
  const permissionShareScreenBtn = document.getElementById('SHARE_SCREEN')
  const permissionShareVideoBtn = document.getElementById('SHARE_VIDEO')
  const permissionShareFileBtn = document.getElementById('SHARE_FILE')
  const permissionSendMessageBtn = document.getElementById('SEND_MESSAGE')
  const permissionRecordBtn = document.getElementById('RECORD')
  const permissionStreamBtn = document.getElementById('STREAM')
  const updatePermissionsSubmitBtn = document.getElementById('submit-permissions')
  const invitationList = document.getElementById('invitation-list')
  const startFilePresentationBtn = document.getElementById('start-file-presentation-btn');
  const stopFilePresentationBtn = document.getElementById('stop-file-presentation-btn');
  const fileInput = document.getElementById('file-presentation-file');

  const permissionButtons = [
    permissionInviteBtn,
    permissionKickBtn,
    permissionUpdatePermissionBtn,
    permissionJoinBtn,
    permissionSendAudioBtn,
    permissionSendVideoBtn,
    permissionShareScreenBtn,
    permissionShareVideoBtn,
    permissionShareFileBtn,
    permissionSendMessageBtn,
    permissionRecordBtn,
    permissionStreamBtn,
  ]

  // Update the login message with the name of the user
  nameMessage.innerHTML = `You are logged in as ${randomName}`
  joinButton.disabled = false

  // Method to update draft permissions array

  let permissionsArray = []

  const updatePermissionsArray = (permission) => {
    if (permissionsArray.includes(permission)) {
      permissionsArray = permissionsArray.filter((p) => p !== permission)
    } else {
      permissionsArray = [...permissionsArray, permission]
    }
  }

  joinButton.onclick = () => {
    // Default conference parameters
    // See: https://docs.dolby.io/interactivity/docs/js-client-sdk-model-conferenceparameters
    let conferenceParams = {
      liveRecording: false,
      rtcpMode: 'average', // worst, average, max
      ttl: 0,
      videoCodec: 'H264', // H264, VP8
      dolbyVoice: true,
    }

    // See: https://docs.dolby.io/interactivity/docs/js-client-sdk-model-conferenceoptions
    let conferenceOptions = {
      alias: conferenceAliasInput.value,
      params: conferenceParams,
    }

    // 1. Create a conference room with an alias
    VoxeetSDK.conference
      .create(conferenceOptions)
      .then((conference) => {
        // See: https://docs.dolby.io/interactivity/docs/js-client-sdk-model-joinoptions
        const joinOptions = {
          constraints: {
            audio: true,
            video: false,
          },
          simulcast: false,
        }

        // 2. Join the conference
        VoxeetSDK.conference
          .join(conference, joinOptions)
          .then((conf) => {
            lblDolbyVoice.innerHTML = `Dolby Voice is ${
              conf.params.dolbyVoice ? 'On' : 'Off'
            }.`

            conferenceAliasInput.disabled = true;
            sendMessageBtn.disabled = false;
            joinButton.disabled = true;
            leaveButton.disabled = false;
            inviteInput.disabled = false;
            inviteButton.disabled = false;
            startVideoBtn.disabled = false;
            startAudioBtn.disabled = true;
            stopAudioBtn.disabled = false;
            startScreenShareBtn.disabled = false;
            startRecordingBtn.disabled = false;
            startFilePresentationBtn.disabled = false;
            permissionButtons.forEach((btn) => {
              btn.disabled = false
            })
            updatePermissionsSubmitBtn.disabled = false
          })
          .catch((err) => console.error(err))
      })
      .catch((err) => console.error(err))
  }

  leaveButton.onclick = () => {
    // Leave the conference
    VoxeetSDK.conference
      .leave()
      .then(() => {
        lblDolbyVoice.innerHTML = ''

        conferenceAliasInput.disabled = false;
        joinButton.disabled = false;
        sendMessageBtn.disabled = true;
        leaveButton.disabled = true;
        inviteInput.disabled = true;
        inviteButton.disabled = true;
        startVideoBtn.disabled = true;
        stopVideoBtn.disabled = true;
        startAudioBtn.disabled = true;
        stopAudioBtn.disabled = true;
        startScreenShareBtn.disabled = true;
        stopScreenShareBtn.disabled = true;
        startRecordingBtn.disabled = true;
        stopRecordingBtn.disabled = true;
        startFilePresentationBtn.disabled = true;
        permissionButtons.forEach((btn) => {
          btn.disabled = true
        })
        updatePermissionsSubmitBtn.disabled = true
      })
      .catch((err) => console.error(err))
  }

  inviteButton.onclick = () => {
    var conference = VoxeetSDK.conference.current
    var externalId = inviteInput.value
    console.warn('id = ' + externalId)
    var participants = [{ name: 'Test User', externalId: externalId, avatarUrl: '' }]
    VoxeetSDK.notification
      .invite(conference, participants)
      .then(() => {
        console.info('Invitation send to: ' + externalId)
      })
      .catch((err) => console.log(err))
  }

  sendMessageBtn.onclick = () => {
    // Send chat message
    VoxeetSDK.command
      .send(messageInput.value)
      .then(() => {
        addChatMessageNode(VoxeetSDK.session.participant, messageInput.value)
      })
      .catch((err) => console.error(err))
  }

  startVideoBtn.onclick = () => {
    // Start sharing the video with the other participants
    VoxeetSDK.conference
      .startVideo(VoxeetSDK.session.participant)
      .then(() => {
        startVideoBtn.disabled = true
        stopVideoBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  stopVideoBtn.onclick = () => {
    // Stop sharing the video with the other participants
    VoxeetSDK.conference
      .stopVideo(VoxeetSDK.session.participant)
      .then(() => {
        stopVideoBtn.disabled = true
        startVideoBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  startAudioBtn.onclick = () => {
    // Start sharing the Audio with the other participants
    VoxeetSDK.conference
      .startAudio(VoxeetSDK.session.participant)
      .then(() => {
        startAudioBtn.disabled = true
        stopAudioBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  stopAudioBtn.onclick = () => {
    // Stop sharing the Audio with the other participants
    VoxeetSDK.conference
      .stopAudio(VoxeetSDK.session.participant)
      .then(() => {
        stopAudioBtn.disabled = true
        startAudioBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  startScreenShareBtn.onclick = () => {
    // Start the Screen Sharing with the other participants
    VoxeetSDK.conference
      .startScreenShare()
      .then(() => {
        startScreenShareBtn.disabled = true
        stopScreenShareBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  stopScreenShareBtn.onclick = () => {
    // Stop the Screen Sharing
    VoxeetSDK.conference.stopScreenShare().catch((err) => console.error(err))
  }

  startRecordingBtn.onclick = () => {
    let recordStatus = document.getElementById('record-status')

    // Start recording the conference
    VoxeetSDK.recording
      .start()
      .then(() => {
        recordStatus.innerText = 'Recording...'
        startRecordingBtn.disabled = true
        stopRecordingBtn.disabled = false
      })
      .catch((err) => console.error(err))
  }

  stopRecordingBtn.onclick = () => {
    let recordStatus = document.getElementById('record-status')

    // Stop recording the conference
    VoxeetSDK.recording
      .stop()
      .then(() => {
        recordStatus.innerText = ''
        startRecordingBtn.disabled = false
        stopRecordingBtn.disabled = true
      })
      .catch((err) => console.error(err))
  }

  // Add permissions to permissions array
  permissionButtons.forEach((btn) => {
    btn.onclick = (e) => {
      updatePermissionsArray(e.target.id)
      e.target.classList.toggle('green')
      console.log(permissionsArray)
    }
  })

  // Submit updating permissions
  updatePermissionsSubmitBtn.onclick = async () => {
    const participantsMap = await VoxeetSDK.conference.current.participants
    const participantsArray = Array.from(participantsMap, ([name, value]) => ({
      name,
      value,
    }))
    const targetParticipant = participantsArray[1].value

    if (targetParticipant) {
      await VoxeetSDK.conference.updatePermissions([
        { participant: targetParticipant, permissions: permissionsArray },
      ])
      permissionsArray = []
      permissionButtons.forEach((btn) => {
        btn.classList.remove('green')
      })
      console.log(targetParticipant)
      alert('Permissions updated')
    } else {
      console.log('Target participant not exist')
    }
  }

  VoxeetSDK.filePresentation.on("started", async e => {
      console.log('FILE PRESENTATION STARTED EVENT DATA:\n', JSON.stringify(e, null, 2))
      const { id, position } = e;
      try {
          const a = await VoxeetSDK.filePresentation.image(position, id)
          console.log('file presentation image src', a)
          addFilePresentationNode(a)
      } catch (e) {
          alert('could not retrieve file presentation image')
          console.error(e)
      }
  })

    VoxeetSDK.filePresentation.on("stopped", async e => {
        console.log('FILE PRESENTATION STOPPED EVENT DATA:\n', JSON.stringify(e, null, 2))
        clearFilePresentationNode();
    })

    VoxeetSDK.filePresentation.on("updated", async e => {
        console.log('FILE PRESENTATION UPDATED EVENT DATA:\n', JSON.stringify(e, null, 2))
        const { id, position } = e;
        try {
            const a = await VoxeetSDK.filePresentation.image(position, id)
            console.log('file presentation image src', a)
            clearFilePresentationNode();
            addFilePresentationNode(a)
        } catch (e) {
            alert('could not retrieve file presentation image')
            console.error(e)
        }
    })

    VoxeetSDK.filePresentation.on("converted", async e => {
        console.log('FILE PRESENTATION CONVERTED EVENT DATA:\n', JSON.stringify(e, null, 2))
        try {
            await VoxeetSDK.filePresentation.start(e)
            startFilePresentationBtn.disabled = true;
            stopFilePresentationBtn.disabled = false;
        } catch (e) {
            console.log('file presentation start error', e)
        }
    })

    startFilePresentationBtn.onclick = () => {
        fileInput.click();
    }

    stopFilePresentationBtn.onclick = async () => {
        await VoxeetSDK.filePresentation.stop()
        clearFilePresentationNode();
        startFilePresentationBtn.disabled = false;
        stopFilePresentationBtn.disabled = true;
    }

    fileInput.addEventListener('change', async (e) => {
        try {
            if (!e.target.files.length) {
                alert('You must choose a file to present!')
                return
            }
            await VoxeetSDK.filePresentation.convert(e.target.files[0])
        } catch (e) {
            console.log('file convert error', e)
        }
    })


    VoxeetSDK.videoPresentation.on('started', e => {
        console.log('VIDEO PRESENTATION STARTED:\n', e)
    })
    VoxeetSDK.videoPresentation.on('stopped', e => {
        console.log('VIDEO PRESENTATION STOPPED:\n', e)
    })
    VoxeetSDK.videoPresentation.on('sought', e => {
        console.log('VIDEO PRESENTATION SOUGHT:\n', e)
    })
    VoxeetSDK.videoPresentation.on('paused', e => {
        console.log('VIDEO PRESENTATION PAUSED:\n', e)
    })
    VoxeetSDK.videoPresentation.on('played', e => {
        console.log('VIDEO PRESENTATION PLAYED:\n', e)
    })
    // invitation handler
    VoxeetSDK.notification.on("invitation", async (e) => {
        console.log('INVITATION EVENT DATA: \n', JSON.stringify(e, null, 2))
        const item = document.createElement("li");
        item.appendChild(document.createTextNode(e.conferenceAlias));
        const acceptButton = createButton('Accept', async () => {
            const c = await VoxeetSDK.conference.fetch(e.conferenceId)
            VoxeetSDK.conference.join(c, { conferenceToken: e.conferenceToken})
                .then((conf) => {
                    lblDolbyVoice.innerHTML = `Dolby Voice is ${conf.params.dolbyVoice ? 'On' : 'Off'}.`;

          conferenceAliasInput.disabled = true
          conferenceAliasInput.value = e.conferenceAlias
          sendMessageBtn.disabled = false
          joinButton.disabled = true
          leaveButton.disabled = false
          inviteInput.disabled = false
          inviteButton.disabled = false
          startVideoBtn.disabled = false
          startAudioBtn.disabled = true
          stopAudioBtn.disabled = false
          startScreenShareBtn.disabled = false
          startRecordingBtn.disabled = false
          startFilePresentationBtn.disabled = true;
          stopFilePresentationBtn.disabled = true;
          permissionButtons.forEach((btn) => {
            btn.disabled = false
          })
          updatePermissionsSubmitBtn.disabled = false
        })
        .catch((err) => console.error(err))
      item.remove()
    })
    const declineButton = createButton('Decline', () => {
      console.log('There is no decline method on JS sdk!')
      item.remove()
    })
    item.appendChild(acceptButton)
    item.appendChild(declineButton)
    invitationList.appendChild(item)
  })
}

// Add a video stream to the web page
const addVideoNode = (participant, stream) => {
  let videoNode = document.getElementById('video-' + participant.id)

  if (!videoNode) {
    videoNode = document.createElement('video')

    videoNode.setAttribute('id', 'video-' + participant.id)
    videoNode.setAttribute('height', 240)
    videoNode.setAttribute('width', 320)
    videoNode.setAttribute('playsinline', true)
    videoNode.muted = true
    videoNode.setAttribute('autoplay', 'autoplay')
    videoNode.style = 'background: gray;'

    const videoContainer = document.getElementById('video-container')
    videoContainer.appendChild(videoNode)
  }

  navigator.attachMediaStream(videoNode, stream)
}

// Remove the video streem from the web page
const removeVideoNode = (participant) => {
  let videoNode = document.getElementById('video-' + participant.id)

  if (videoNode) {
    videoNode.srcObject = null // Prevent memory leak in Chrome
    videoNode.parentNode.removeChild(videoNode)
  }
}

// Add a new participant to the list
const addChatMessageNode = (participant, message) => {
  let messageNode = document.createElement('li')
  messageNode.innerText = `${participant.info.name}\n${message}`

  const messagesList = document.getElementById('messages-list')
  messagesList.appendChild(messageNode)
}

const addFilePresentationNode = (fileSrc) => {
    const img = document.createElement('img')
    img.src = fileSrc
    img.width = 200

    const vp = document.getElementById('filepresentation-container');
    vp.appendChild(img)
}

const clearFilePresentationNode = () => {
    const img = document.querySelector('#filepresentation-container img')
    img.remove();
}

// Add a new participant to the list
const addParticipantNode = (participant) => {
  // If the participant is the current session user, don't add them to the list
  if (participant.id === VoxeetSDK.session.participant.id) return

  let participantNode = document.createElement('li')
  participantNode.setAttribute('id', 'participant-' + participant.id)
  participantNode.innerText = `${participant.info.name}`

  const participantsList = document.getElementById('participants-list')
  participantsList.appendChild(participantNode)
}

// Remove a participant from the list
const removeParticipantNode = (participant) => {
  let participantNode = document.getElementById('participant-' + participant.id)

  if (participantNode) {
    participantNode.parentNode.removeChild(participantNode)
  }
}

// Add a screen share stream to the web page
const addScreenShareNode = (stream) => {
  let screenShareNode = document.getElementById('screenshare')

  if (screenShareNode) {
    return alert('There is already a participant sharing a screen!')
  }

  screenShareNode = document.createElement('video')
  screenShareNode.setAttribute('id', 'screenshare')
  screenShareNode.autoplay = 'autoplay'
  navigator.attachMediaStream(screenShareNode, stream)

  const screenShareContainer = document.getElementById('screenshare-container')
  screenShareContainer.appendChild(screenShareNode)
}

// Remove the screen share stream from the web page
const removeScreenShareNode = () => {
  let screenShareNode = document.getElementById('screenshare')

  if (screenShareNode) {
    screenShareNode.srcObject = null // Prevent memory leak in Chrome
    screenShareNode.parentNode.removeChild(screenShareNode)
  }

  const startScreenShareBtn = document.getElementById('start-screenshare-btn')
  startScreenShareBtn.disabled = false

  const stopScreenShareBtn = document.getElementById('stop-screenshare-btn')
  stopScreenShareBtn.disabled = true
}

const createButton = (text, onClickFn) => {
  const button = document.createElement('button')
  button.innerHTML = text || 'Button'
  button.onclick = onClickFn
  return button
}